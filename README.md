# Helloworld Pad

A single switch macropad to start learning handwired keyboard with a Pro-micro.

![Hello World Pad](doc/helloworld_pro_micro.jpg)

## Electronic

The cherry MX switch (the white one) is connected to `A2` and `A3` pin of the Arduino whose are `F4` and `F5` in `qmk`.
The black one is connected between `RESET` and `GND` and is used to *reset* the Pro-micro in order to flash it.

Here is the pinout of the Pro Micro board:

![pro micro pinout](https://i.pinimg.com/originals/a5/04/57/a504570b03287da7cdd419270ec49603.jpg)

Source: https://i.pinimg.com/originals/a5/04/57/a504570b03287da7cdd419270ec49603.jpg

## Flash Instructions

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

### Import QMK firmware

```
git clone https://gitlab.com/coliss86/helloworld_promicro
git clone -b 0.17.5 https://github.com/qmk/qmk_firmware
cd qmk_firmware/keyboards
ln -s ../../helloworld_promicro
```

### Flash the Atmega

- Compile the firmware:

```
qmk compile -kb helloworld_promicro -km default
```

- to upload, hit twice in a second the *reset* switch to put the Atmega in bootloader mode and launch :
```
qmk flash -kb helloworld_promicro -km default
```

Here is a normal output :

```
Making helloworld_promicro with keymap default and target avrdude

avr-gcc (GCC) 5.4.0
Copyright (C) 2015 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Size before:
   text	   data	    bss	    dec	    hex	filename
      0	  13268	      0	  13268	   33d4	.build/helloworld_promicro_default.hex

Copying helloworld_promicro_default.hex to qmk_firmware folder                                 [OK]
Checking file size of helloworld_promicro_default.hex                                          [OK]
 * The firmware size is fine - 13268/28672 (46%, 15404 bytes free)
Detecting USB port, reset your controller now........
Device /dev/ttyACM0 has appeared; assuming it is the controller.
Waiting for /dev/ttyACM0 to become writable.

Connecting to programmer: .
Found programmer: Id = "CATERIN"; type = S
    Software Version = 1.0; No Hardware Version given.
Programmer supports auto addr increment.
Programmer supports buffered memory access with buffersize=128 bytes.

Programmer supports the following devices:
    Device code: 0x44

avrdude: AVR device initialized and ready to accept instructions

Reading | ################################################## | 100% 0.00s

avrdude: Device signature = 0x1e9587 (probably m32u4)
avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
         To disable this feature, specify the -D option.
avrdude: erasing chip
avrdude: reading input file ".build/helloworld_promicro_default.hex"
avrdude: input file .build/helloworld_promicro_default.hex auto detected as Intel Hex
avrdude: writing flash (13268 bytes):

Writing | ################################################## | 100% 1.00s

avrdude: 13268 bytes of flash written
avrdude: verifying flash memory against .build/helloworld_promicro_default.hex:
avrdude: load data flash data from input file .build/helloworld_promicro_default.hex:
avrdude: input file .build/helloworld_promicro_default.hex auto detected as Intel Hex
avrdude: input file .build/helloworld_promicro_default.hex contains 13268 bytes
avrdude: reading on-chip flash data:

Reading | ################################################## | 100% 0.12s

avrdude: verifying ...
avrdude: 13268 bytes of flash verified

avrdude: safemode: Fuses OK (E:CB, H:D8, L:FF)

avrdude done.  Thank you.
```

## Update qmk

To update qmk, go to the `qmk_firmware` and run the following commands:
```
cd qmk_firmware
git fetch -a
git checkout <new_tag>
make git-submodule
```
